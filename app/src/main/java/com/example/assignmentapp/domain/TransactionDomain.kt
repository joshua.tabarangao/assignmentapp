package com.example.assignmentapp.domain

import android.annotation.SuppressLint
import android.text.format.DateFormat
import com.example.assignmentapp.model.TransactionItem
import java.util.*
import kotlin.random.Random

// sample repository generate transaction items
class TransactionDomain {

    @SuppressLint("SimpleDateFormat")
    fun requestTransaction(): MutableList<TransactionItem> {

        val list= mutableListOf<TransactionItem>()
        val calendar = Calendar.getInstance()
        var date = calendar.get(Calendar.DAY_OF_MONTH)

        while (date!=0)
        {
            calendar.set(Calendar.DAY_OF_MONTH,date)

            var itemNumber = Random.nextInt(1,8)
            while (itemNumber !=0){

                val item = TransactionItem(
                    transactionId = UUID.randomUUID().toString().substring(0,6),
                    amount = Random.nextDouble(1.00, 1000.00),
                    date = "${DateFormat.format("MM-dd-yyyy",calendar)}")

                list.add(item)

                itemNumber--
            }
            date--
        }
        return list
    }
}
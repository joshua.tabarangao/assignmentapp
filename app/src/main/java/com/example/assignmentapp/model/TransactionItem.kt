package com.example.assignmentapp.model

data class TransactionItem(val transactionId : String, val amount : Double, val date : String)
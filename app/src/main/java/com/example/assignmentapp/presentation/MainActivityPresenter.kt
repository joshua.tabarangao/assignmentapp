package com.example.assignmentapp.presentation

import com.example.assignmentapp.domain.TransactionDomain

class MainActivityPresenter(private val view: MainActivityContract.View) :
    MainActivityContract.Presenter {

    private val transactionDomain = TransactionDomain()

    init {
        view.initViews()
        requestTransaction()
    }

    override fun requestTransaction() {
        view.onTransactionLoaded(transactionDomain.requestTransaction())
    }
}
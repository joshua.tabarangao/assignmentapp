package com.example.assignmentapp.presentation

import com.example.assignmentapp.model.TransactionItem

interface MainActivityContract {

    interface View {
        fun initViews()
        fun onTransactionLoaded(list: MutableList<TransactionItem>)
    }

    interface Presenter {
        fun requestTransaction()
    }
}
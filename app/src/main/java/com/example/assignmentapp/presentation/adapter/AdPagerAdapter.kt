package com.example.assignmentapp.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.assignmentapp.R
import com.google.android.ads.nativetemplates.TemplateView
import com.google.android.gms.ads.AdLoader
import com.google.android.gms.ads.AdRequest

class AdPagerAdapter : RecyclerView.Adapter<AdPagerAdapter.AdViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdViewHolder {
        return AdViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_ad_view, parent, false)
        )
    }

    override fun onBindViewHolder(holder: AdViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemCount(): Int {
        return 4
    }

    class AdViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(index: Int) {
            val builder =
                AdLoader.Builder(itemView.context, "ca-app-pub-3940256099942544/2247696110")
            builder.forUnifiedNativeAd {
                val templateView = itemView.findViewById<TemplateView>(R.id.my_template)
                templateView.setNativeAd(it)
            }

            val adRequest = AdRequest.Builder().build()

            builder.build().loadAds(adRequest, index  + 1)
        }
    }
}
package com.example.assignmentapp.presentation

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Menu
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager2.widget.ViewPager2
import com.example.assignmentapp.R
import com.example.assignmentapp.model.TransactionItem
import com.example.assignmentapp.presentation.adapter.AdPagerAdapter
import com.example.assignmentapp.presentation.adapter.TransactionAdapter
import com.example.assignmentapp.util.PagerIndicator
import com.google.android.gms.ads.MobileAds
import java.util.*
import kotlin.math.log

class MainActivity : AppCompatActivity(), MainActivityContract.View {

    lateinit var adapter: TransactionAdapter

    lateinit var presenter: MainActivityPresenter

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)

        val menuActionItem = menu?.findItem(R.id.search)
        val searchView = menuActionItem?.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                Log.i("testing", "onQueryTextSubmit: $query")
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                Log.i("testing", "onQueryTextChange: $newText")
                adapter.filter(newText!!)
                return true
            }
        })
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = MainActivityPresenter(this)
    }

    override fun initViews() {
        supportActionBar?.title = getString(R.string.lbl_transaction_history)

        // initialize view pager
        val adViewPager = findViewById<ViewPager2>(R.id.ad_pager)

        // dot indicator
        val pagerIndicator = findViewById<PagerIndicator>(R.id.pager_indicator)

        // initialize admob
        MobileAds.initialize(this) {
            adViewPager.adapter?:let {
                adViewPager.adapter = AdPagerAdapter()
                pagerIndicator.initDot(adViewPager.adapter!!.itemCount)
            }
        }

        // listener for ad page change
        adViewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                pagerIndicator.setSelectedDot(position)
            }
        })

        val swipeLayout = findViewById<SwipeRefreshLayout>(R.id.swipe_layout)
        swipeLayout.setOnRefreshListener {
            Handler(Looper.getMainLooper()).postDelayed({
                swipeLayout.isRefreshing = false
                presenter.requestTransaction()
            }, 500)
        }

        val rvTransaction = findViewById<RecyclerView>(R.id.rv_transaction)
        adapter = TransactionAdapter()
        rvTransaction.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = this@MainActivity.adapter
        }
    }

    override fun onTransactionLoaded(list: MutableList<TransactionItem>) {
        adapter.setData(list)
    }
}
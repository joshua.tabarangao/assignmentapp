package com.example.assignmentapp.presentation.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.assignmentapp.R
import com.example.assignmentapp.model.TransactionItem
import com.example.assignmentapp.util.CustomDiffChecker
import java.text.SimpleDateFormat
import java.util.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "SimpleDateFormat", "SetTextI18n")
class TransactionAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_TYPE_HEADER = 0
        const val VIEW_TYPE_ITEM = 1
    }

    private val mList = mutableListOf<TransactionItem>()
    private val filteredList = mutableListOf<Any>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_ITEM -> ViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_transaction_detail, parent, false)
            )
            else -> HeaderViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_transaction_header, parent, false)
            )
        }
    }

    override fun getItemCount() = filteredList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            holder.onBind(filteredList[position] as TransactionItem)
        } else if (holder is HeaderViewHolder) {
            holder.onBind(filteredList[position] as DateHeader)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (filteredList[position]) {
            is TransactionItem -> VIEW_TYPE_ITEM
            else -> VIEW_TYPE_HEADER
        }
    }

    fun setData(list: MutableList<TransactionItem>) {
        mList.clear()
        mList.addAll(list)
        submitUpdate(list)
    }

    fun filter(queryString: String) {
        val list = if (queryString.isNotBlank()) {
            mList.filter {
                it.transactionId.contains(queryString)
            }.toMutableList()
        }else mList
        submitUpdate(list)
    }

    private fun submitUpdate(list: MutableList<TransactionItem>) {
        val temp = mutableListOf<Any>()
        var tempDate = ""
        list.forEach {
            if (tempDate != it.date) {
                tempDate = it.date
                temp.add(DateHeader(tempDate))
            }
            temp.add(it)
        }

        val result = object : CustomDiffChecker<Any>(filteredList, temp) {
            override fun areItemTheSame(oldItem: Any, newItem: Any): Boolean {
                if (oldItem is DateHeader && newItem is DateHeader) {
                    return true
                } else if (oldItem is TransactionItem && newItem is TransactionItem) {
                    return true
                }
                return false
            }

            override fun areContentTheSame(oldItem: Any, newItem: Any): Boolean {
                if (oldItem is DateHeader && newItem is DateHeader) {
                    return oldItem.date == newItem.date
                } else if (oldItem is TransactionItem && newItem is TransactionItem) {
                    return oldItem.amount == newItem.amount && oldItem.date == newItem.date
                }
                return false
            }
        }.calculate()

        filteredList.clear()
        filteredList.addAll(temp)
        result.dispatchUpdatesTo(this)
    }


    data class DateHeader(val date: String, var showChild: Boolean = false)

    inner class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun onBind(dateHeader: DateHeader) {

            val parser = SimpleDateFormat("MM-dd-yyyy")

            val today = Calendar.getInstance()
            val itemDate = Calendar.getInstance().apply { time = parser.parse(dateHeader.date) }

            val label = when {
                today.get(Calendar.DATE) == itemDate.get(Calendar.DATE) -> {
                    itemView.context.getString(R.string.lbl_today)
                }
                today.get(Calendar.DATE) - itemDate.get(Calendar.DATE) == 1 -> {
                    itemView.context.getString(R.string.lbl_yesterday)
                }
                else -> SimpleDateFormat("dd MMM, yyyy").format(parser.parse(dateHeader.date))
            }
            itemView.findViewById<TextView>(R.id.tv_date).text = label
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("")
        fun onBind(transactionItem: TransactionItem) {
            itemView.findViewById<TextView>(R.id.tv_amount).text =
                "PHP ${String.format("%,.2f", transactionItem.amount)}"

            itemView.findViewById<TextView>(R.id.tv_transaction_number).text = "Transaction #: ${transactionItem.transactionId}"
        }
    }
}
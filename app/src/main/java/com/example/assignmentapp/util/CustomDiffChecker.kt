package com.example.assignmentapp.util

import androidx.recyclerview.widget.DiffUtil

abstract class CustomDiffChecker<T>(
    private val oldList: MutableList<T>,
    private val newList: MutableList<T>
) : DiffUtil.Callback() {

    abstract fun areItemTheSame(oldItem: T, newItem: T): Boolean

    abstract fun areContentTheSame(oldItem: T, newItem: T): Boolean

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return areItemTheSame(oldList[oldItemPosition], newList[newItemPosition])
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return areContentTheSame(oldList[oldItemPosition],newList[newItemPosition])
    }

    fun calculate() = DiffUtil.calculateDiff(this)
}
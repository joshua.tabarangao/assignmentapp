package com.example.assignmentapp.util

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.view.children
import com.example.assignmentapp.R

class PagerIndicator : LinearLayout {


    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    fun initDot(dotCount: Int) {
        val dotSize = context.resources.getDimensionPixelSize(R.dimen.dp_8)
        for (i in 0 until dotCount) {
            val child = View(context)

            val params = LayoutParams(dotSize, dotSize)
            (params as MarginLayoutParams).marginEnd = 10
            child.background =
                ContextCompat.getDrawable(context, R.drawable.selector_viewpager_indicator)
            addView(child, params)
        }
    }

    fun setSelectedDot(position: Int) {
        children.forEach {
            it.isEnabled = indexOfChild(it) == position
        }
    }

}